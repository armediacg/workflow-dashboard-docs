.. image:: src/img/wd_full_logo.png

Welcome to Workflow Dashboard for M-Files documentation
=======================================================
Version 0.22.54

*Last update on 09th Jan. 2023*

Guide
^^^^^
.. toctree::
    :maxdepth: 2

    src/whats-new.rst
    src/installation.md
    src/configuration.rst
    src/vault-config.rst
    src/workflow-config.rst
    src/transition-config.rst
    src/state-config.rst
    src/state-shapes.rst
    src/arrow-types.rst
    src/client-view.rst
    src/tooltip.rst
    src/json-syntax.rst
    src/change-state.rst
    src/markdown.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
