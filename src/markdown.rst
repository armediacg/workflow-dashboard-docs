Markdown Feature
================

What is Markdown ?
------------------
*"Markdown is a lightweight markup language that you can use to add formatting elements to plaintext text documents. Created by John Gruber in 2004, Markdown is now one of the world’s most popular markup languages"*.
Source `Markdown Getting Started`_

How to use Markdown in Workflow Dashboard ?
-------------------------------------------

Workflow Dahsboard supports markdown basic syntax in **workflow**, **state** and **transition** descriptions, with this feature, you can customize your description to make them more organized and formatted, you can for instance:

- Set different levels of heading
- Add code blocks
- Add ordered and unordered lists
- Add external links
- Add images or icons
- And more

To do so, simply use Markdown Syntax in your workflow, state or transition description, then update metadata cache in Workflow Dashboard configuration, see :ref:`Update Workflows`
For more information about markdown syntax, please refer to `Markdown's Basic Syntax`_ page.

Markdown Example
................

Here a basic sample of markdon set on a state:

.. image:: img/markdown.JPG


Here the full text description using markdown

.. code-block::

    #Alpha State Heading
    This is a small **Markdown** *sample*
    ## Ordered list
    1. list item
    2. list item
    ## Unordered list
    - list item
    - list item

    ## Block quotes
    > #### The quarterly results look great!
    >
    > - Revenue was off the chart.
    > - Profits were higher than ever.
    >
    >  *Everything* is going according to **plan**.

    `and sample code support`

And here, you can see the result when you hover on the state

.. image:: img/markdown-res.jpg

Markdown and Configuration Combination
--------------------------------------

You can combine markdown syntax and JSON configuration in any description.
If the JSON configuration has no errors, it will be removed from descriptions and applyed during graph render and the markdown will be parsed.

Here a basic sample of markdown and json configuration on a state, with correct and incorrect JSON

.. code-block::

    #Delta Description
    Note that the **json** configuration of this state has been removed {shape:document, color:#f80, fill:true, stroke:10, size:18, style:bi} from state description
    ##What happens when config is wonrg ?
    Simply, it will be ignored

    `{shape:"rect", color:#f80, fill:true, stroke:10, size:18, style:bi}`


And the result is in the image below, note that the first json configuration was removed from description and applied to the state, but the secound was ignored due to the double quotes, and was not applyed and not removed from the description

.. image:: img/md-json.jpg



.. _`Markdown Getting Started`: https://www.markdownguide.org/getting-started/
.. _`Markdown's Basic Syntax`: https://www.markdownguide.org/basic-syntax/