Configuration
=============

**Workflow Dashboard** has three levels of configurations:

1. :ref:`Global Configuration`
2. :ref:`Workflow Configuration`
3. :ref:`Transition Configuration`
4. :ref:`State Configuration`

The global configuratuion is for all workflows of the vault
The workflow configuration is for specific workflow and overrides the global configuration
The transition configuration is for specific transition and overrides the workflow configuration
The state configuration is for specific state and overrides the workflow configuration

