State Configuration
^^^^^^^^^^^^^^^^^^^

State Configuration allow you to override default state appearance set in :ref:`Global Configuration` AND :ref:`Workflow Configuration` for a given state.
To set state configuration, simply add your configuration in state description using simplified :ref:`Simplified JSON Syntax`.

.. warning:: After you have made changes to state, you'll need to update metadata cache (see :ref:`Update Workflows`).

State Appearence 
................
In this table you'll find all possible options that you can set for your state:

.. csv-table:: 
    :file: tables/state-config.csv
    :widths: 10 15 75
    :delim: ;
    :header-rows: 1

.. tip:: To hide the **no state** of a given workflow, set the value of ``noState`` color to ``#h`` in the workflow configuration, see :ref:`States options` in workflow configuration section.

Example
.......

This configuration set in state description will force this state shape to ``document`` and overrides all default values for color, stroke, size and text style:

    ``{shape:document, color:#37aad4, stroke:5, size:14, style:bi}``

.. image:: img/state-config.jpg


And this is the resulting graph, note that the state color overrides the ``Selectable`` color, to avoide this behavior and override only default color, add the option ``forceColor:false`` in the state configuration.

.. image:: img/state-config-result.jpg


Example of a hidden state
.........................
In the image below, the left graph is the same as the right one where the **State C** has been hidden using the configuration ``{hidden:true}``

.. image:: img/hidden-state.png

Change State Label
..................
In some situations, you may need to use another label for your state than the default Name, for example, long names will generate long shapes and it's better to use a shorter label or use new lines.
In your label you can use some special characters (like comma or double quotes), for this reason, the JSON syntax may not work, or can be tedious to set.
If you want to change your default state label, simply define you new label in state description outside your JSON configuration and enclose it with double percent symbol, for example:
    ``%%My label%%``

In the example below, the title was too long and we used another label with new line to make short rectabgle, we also added comma and percent symbole intentionally

.. image:: img/state-label.png

And the result is

.. image:: img/state-label-result.png