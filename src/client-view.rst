Client View
===========
In M-Files client, When you select an object which has a workflow, you'll find the **Workflow Dashboard** tab in the metadata card panel.

This tab contains:
  - The workflow name
  - The state colors legend
  - Buttons bar
  - Workflow graph view

In this tab you'll be able to :
  - Cusomize the view and zoom
  - Darag and Zoom in/out the graph using the mouse wheel
  - Change dynamically the graph layout
  - Change dynamically the transition line type
  - Change object state by clicking on available selectable states
  - See state description, actions and conditions when ouse hover the state
  - See transition description and triggers when mouse hover the info icon
  - See workflow description
  - Get the denug data when necessary

.. image:: img/client_view.png


View buttons
------------
.. list-table:: 
    :widths: 10 90
    :header-rows: 1

    * - Button
      - Action
    * - .. image:: icon/grid.png
      - Whether to show the grid or not 
    * - .. image:: icon/dark-mode.png
      - Whether to use dark or light background
    * - .. image:: icon/zoom-graph.png
      - Trigger a zoom in such a way to fit all the graph
    * - .. image:: icon/zoom-state.png
      - Trigger a zoom in such a way to focus on current object's state
    * - .. image:: icon/zoom-100.png
      - Trigger a zoom to a 100% scale


Layout buttons
--------------
.. list-table:: 
    :widths: 5 15 15 65
    :header-rows: 1
    
    * - Button
      - JSON name
      - JSON values
      - Action
    * - .. image:: icon/direction.png
      - ``direction``
      - ``TB``, ``LR``
      - Switch the graph layout direction from/to top to bottom/left to right 
    * - .. image:: icon/align.png
      - ``align``
      - ``NONE``, ``UR``, ``DR``, ``DL``, ``UL``
      - Toggle graph alignment layout values
    * - .. image:: icon/long-path.png
      - ``tree``
      - - ``long`` or ``l``
        - ``tight`` or ``t``
      - Toggle graph tree layout type values
    * - .. image:: icon/acyclicer.png
      - ``acyclicer``
      - ``true``, ``false``
      - Whether to enable cyclic layout or not, works only for cyclic graphs


Transition buttons
------------------
.. list-table:: 
    :widths: 5 15 15 65
    :header-rows: 1
    
    * - Button
      - JSON name
      - JSON value
      - Action
    * - .. image:: icon/history-path.png
      -
      -
      - Whether to highlight the history path or not
    * - .. image:: icon/link-label.png
      -
      -
      - Whether to shows transition labels or not
    * - .. image:: icon/step-edge.png
      - - ``linkType`` for Workflows
        - ``type`` for Transitions
      - ``step`` or ``s``
      - Toggles transitions interponation types
    * - .. image:: icon/linear-edge.png
      - Same as above
      - ``linear`` or ``l``
      - Description
    * - .. image:: icon/bezier-edge.png
      - Same as above
      - ``basis`` or ``b``
      - Description

Other buttons
-------------
.. list-table:: 
    :widths: 10 90
    :header-rows: 1
     
    * - Button
      - Action
    * - .. image:: icon/refresh.png
      - Reloads the workflow data and redraws the graph using default settings
    * - .. image:: icon/wf-info.png
      - Displays workflow description
    * - .. image:: icon/debug.png
      - Displays the debug window, this button is disabled if the debug option is disabled, see :ref:`Enable debug mode`

