Workflow Configuration
======================

These configurations allows you to override the default graph layout, state and transition appearance set in :ref:`Global Configuration` for a given workflow.
To set workflow configuration, simply add your configuration in workflow description using :ref:`Simplified JSON Syntax`.

.. warning:: After you have made changes to workflow, you'll need to update metadata cache (see :ref:`Update Workflows`).

Layout options
--------------

.. list-table:: 
    :widths: 10 15 75
    :header-rows: 1

    * - Option
      - Values
      - Description
    * - ``direction``
      - ``TB`` or ``LR``
      - Define current workflow graph direction: Top to Bottom or Left To Right
    * - ``align``
      - - ``NONE``
        - ``UR``
        - ``DR``
        - ``DL``
        - ``UL``
      - Define current workflow graph alignment, where: **U** is for Up, **D** is for Down, **R** is for Right and **L** is for Left
    * - ``tree``
      - - ``long`` or ``l``
        - ``tight`` or ``t``
      - Define current workflow graph tree layout type
    * - ``acyclicer``
      - ``true`` or ``false``
      - Whether to enable cyclic layout for current workflow or not, works only for cyclic graphs

States options
--------------

.. csv-table:: 
    :file: tables/workflow-config.csv
    :widths: 10 15 75
    :delim: ;
    :header-rows: 1

Transitions options
-------------------

.. list-table:: 
    :widths: 10 30 60
    :header-rows: 1

    * - Option
      - Values
      - Description
    * - ``linkType``
      - - ``basis`` or ``b``
        - ``step`` or ``s``
        - ``linear`` or ``l``
      - Define the interpolation type used for current workflow's transitions. Default value is ``basis``
    * - ``linkStyle``
      - - ``solid``
        - ``dotted`` or ``dot``
        - ``dashed`` or ``dash``
      - Define the stroke style used for current workflow's transitions. Default is ``dashed`` for automatic transitions and ``solid`` otherwise.
    * - ``linkHead``
      - - ``arrow`` or ``a``
        - ``vee`` or ``v``
        - ``triangle`` or ``t``
        - ``sTriangle`` or ``st`` (solid)
        - ``square`` or ``s``
        - ``sSquare`` or ``ss`` (solid)
        - ``circle`` or ``c``
        - ``sCircle`` or ``sc`` (solid)
        - ``none`` or ``n``
      - Define current workflow transition arrows. Default is ``arrow``

Example
.......

This configuration set in workflow description will set all its state shapes to ``rectangle`` and change colors of ``current`` and ``selectable`` states:

    ``{shape:rect, current:#ff8c00, selectable:#f00}``

.. image:: img/wf-config.jpg


And this is the resulting graph for this workflow only:

.. image:: img/wf-config-result.jpg
