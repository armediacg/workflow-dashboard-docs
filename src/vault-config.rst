
Global Configuration
^^^^^^^^^^^^^^^^^^^^

Global configuration allows you to set default options for all workflows and states in the vault and default view settings.

In M-Files Admin go to ``Configuration``, select ``Other Applications``, **Workflow Dashboard** will be in listed applications.
You can access application configuration from the right pane tabs, or by expanding the application tree in the left pane

* :ref:`Update Workflows`
* :ref:`Allow State Transitions`
* :ref:`Default transition comment`
* :ref:`Default View Settings`
* :ref:`Default Layout Settings`
* :ref:`Default Transition Settings`
* :ref:`Default State Settings`
* :ref:`Default State Shape`
* :ref:`Enable debug mode`

.. image::  img/config.png

Update Workflows
----------------

Workflow Dashboard uses WorkflowAdmin cached data, when you made changes to workflow, state or state transition in M-Files admin, these changes will not appear in client view until the metadata cache is refreshed.
To refresh Workflow Dashboard's metadata cache, click on **Update Workflows** button, which is on the top right corner of Workflow Dashboard configuration.


Allow State Transitions
-----------------------

When set to ``Yes`` this will allow users to set object state into next transition using Workflow Dashboard by clicking on the state in the graph.

Default transition comment
--------------------------
You can define a default comment when a transition is triggered, this comment will fill the State Transition dialog each time you click on state, ofcourse this comment can be changed in the dialog


Default View Settings
---------------------
In this section you can set default view options:

.. image:: img/config_view.JPG

View Options
............

.. csv-table:: 
    :file: tables/view-options.csv
    :widths: 20 20 60
    :delim: ;
    :header-rows: 1

.. warning:: The ``Show history path`` and ``Show transition names`` options are moved to :ref:`Default Transition Settings` section. 
            The ``Default graph direction`` option is moved to :ref:`Default Layout Settings` section.

Default Layout Settings
-----------------------
This section allows you to define the default layout the graph should be drawn

.. image:: img/config_layout.png

Layout Options
..............

.. csv-table:: 
    :file: tables/layout-options.csv
    :widths: 10 30 60
    :delim: ;
    :header-rows: 1

Default Transition Settings
---------------------------
You can choose the default way that transition links will be drawn:

.. image:: img/config_trans.jpg

Transition Optioins
...................

.. csv-table:: 
    :file: tables/link-config.csv
    :widths: 10 40 50
    :delim: ;
    :header-rows: 1

Default State Settings
----------------------
In this section, you can set appearance for different states according on theire statuses:

.. image:: img/config_colors.png

State Settings Description
..........................

.. csv-table:: 
    :file: tables/global-colors.csv
    :widths: 15 20 65
    :delim: ;
    :header-rows: 1

Default State Shape
-------------------
Here you can select the dafault state shape for all workflows, there are actually 14 different shapes plus the ``Automatic`` option which will define a shape according to state actions and/or conditions.
For more details about shapes and automatic shape selection see :ref:`Available State Shapes`

Enable debug mode
-----------------
When enabled, a button will be added in client tab to show or hide the debug window.
The debug window can be helpful to:

1. Log any errors which may not let the graph draws well
2. Shows workflow and state IDs with aliasses on state tooltip
3. Logs some useful informations which may be sent for technical support

