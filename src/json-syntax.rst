Simplified JSON Syntax
----------------------
Workflow and State customization uses `JSON`_ syntax with some changes to make it simpler and easier:

- Configuration settings should begins with **left brace** ``{`` and ends with **right brace** ``}``.
- Options should be separated with **comma** ``,``, except for unique option or last one.
- To set an option, you simply write the option name and its value, separated with **colon** ``:``, for instance: ``{shape:circle}``, ``{shape:roundRect, color:#f8c}``
- Do not use **double quotes** ``"`` as default JSON syntax do.
- Boolean values are ``true`` or ``false``.
- Colors uses hexadecimal notation, starting with **hashtag** ``#`` symbol fellowed by **6** hexadecimal digits, for inctance: ``#F506B7``
- A color code can be simplified to **3** digits when possible, for instance: ``#CC9922`` can be simplified to ``#C92``

You can read about color code at this link `Colors HEX`_.

.. _JSON: https://www.json.org/json-en.html
.. _Colors HEX: https://www.w3schools.com/colors/colors_hexadecimal.asp
