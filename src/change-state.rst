Change State
============
When :ref:`Allow State Transitions` feature in :ref:`Global Configuration` is enabled (set to ``Yes``), you can trigger a state transition (change object state) to next possible state by clicking on the state in the graph.
A dialog will be shown like M-Files **Change State** dialog to let you:

- Customize the transition comment.
- Cancel the transition
- Validate the transition

.. image:: img/trigger-trans.jpg


Change State Error
------------------

This dialog mimics the default M-Files **Change State** dialog, if the state can not be set (for instance some conditions are not satisfied) then an error dialog will be displayed explaining why the transition can not be set.

.. image:: img/trigger-err.JPG
