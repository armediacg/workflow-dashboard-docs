Tooltip Information
===================

The tooltip information allows you to read workflow, state or transition description and if available extra informations such as triggers, conditions and actions when available.

* The tooltip for workflow is shown in the workflow info icon in the buttons bar
* The tooltip for state is shown when you hover a state
* The tooltip for transitions is shown when you hover the transition info icon. Transition info icon is represented by two types of icons
    * Green info |infogreen| for basic description
    * Blue info |infoblue| if the transition hase automatic triggers

Customizing Tooltip
...................

You can customize the tooltip information using the :ref:`Markdown Feature`

**Example**

.. image:: img/transition_info.png


.. |infogreen| image:: icon/info_green.png
.. |infoblue| image:: icon/info_blue.png