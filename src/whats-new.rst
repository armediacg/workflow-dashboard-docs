What's new in version 0.22.54 ?
===============================
In this new version, Workflow Dashboard brings many changes, new amazing features and improuvements.
Last update on 10 Jan. 2023

Client Dashboard
.................
* Better grid contrast
* Animate the graph when changing graph direction
* Transition labels are now centered properly
* Added the Transition info icons for description and/or triggers
* Added tooltip to transitions
* New interactive buttons to change the graph layout
* New interactive buttons to change the transtition interpolation type
* Cool animations when changing layout and transitions
* Added "Reload" button to refresh workflow and reset to its default settings
* Added the possibility to remove the color states legend ()

For more informations, see :ref:`Client View`

Admin Configuration
...................
* Added Default Layout Settings
* Added Default Transitin Settings
* The ``Default graph direction`` was moved from view section to layout section
* The ``Show history path`` and ``Show transition names``
* Added the possibility to hide the **no state** by default in the State Settings section

For more informations, see :ref:`Global Configuration`

Layout
......
* Added graphe alignment
* Added option to change tree type
* Added option to choose cyclic graphe type

For more informations, see :ref:`Layout Options`

Workflow
........
* Now you can set workflow graph direction
* Added possibility to set workflow layout
* Added option to set workflow alignment
* Added option to set workflow tree type
* Added option to set workflow cyclyc or not

For more informations, see :ref:`Workflow Configuration`

Transitions
...........
* A new feature for transitions, just like workflows and states, now you can customize transition in its description using the :ref:`Simplified JSON Syntax`
* Two new line interpolation types
* Two new line styles
* Seven (7) new arrow types
* The possibility to remove the arrow from the transition

For more informations, see :ref:`Transition Configuration`

States
......
* Added six (6) new shapes for a total of 20 state shapes (see :ref:`Available State Shapes`)
* Added the possibility to remove a given state from the graph
* Added the possibility to change the default label of a given state
* The shapes ``offPageUp`` and ``offPageDown`` has been redrawn for better transition join
* The corner radius of the shape ``roundRect`` has been slightly reduced

For more informations, see :ref:`State Configuration`

Tooltips
........
* Changed the Title color to M-Files blue color
* The font size has been reduced a little bit for more space
* Added default text description for transition trigger types

For more information, see :ref:`Tooltip Information`

General
.......
* Most of the client application code has been changed for better performances
* **Bug fix**: Licence issue which needs to restart the vault fixed
* **Bug fix**: Removed M-Files Error message dialog when license is not installed or expired
* Other small bug fixes
* Other code improuvements in server application

Features Request, Other questions ?
...................................
Form more details, feature request or any other queston, please don't hesitate to `emails us`_

.. _emails us: contact@workflow-dashboard.com