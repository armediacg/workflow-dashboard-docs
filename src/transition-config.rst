
Transition Configuration
^^^^^^^^^^^^^^^^^^^^^^^^

These configurations allows you to override the default transition line interpolation set in :ref:`Global Configuration` AND :ref:`Workflow Configuration` for a given transition.
To set transition configuration, simply add your configuration in transition description using :ref:`Simplified JSON Syntax`.

.. warning:: After you have made changes to transition, you'll need to update metadata cache (see :ref:`Update Workflows`).

In this table you'll find all possible options that you can set for your transition:

.. list-table:: 
    :widths: 10 30 60
    :header-rows: 1

    * - Option
      - Values
      - Description
    * - ``type``
      - - ``basis`` or ``b``
        - ``step`` or ``s``
        - ``linear`` or ``l``
      - Define the interpolation type used for current transition. Default value is ``basis``
    * - ``style``
      - - ``solid``
        - ``dotted`` or ``dot``
        - ``dashed`` or ``dash``
      - Define the stroke style used for current transition, this option won't work on automatic transitions and it's set to ``dashed``, default is ``solid`` if it's not automatic transition.
    * - ``head``
      - - ``arrow`` or ``a``
        - ``vee`` or ``v``
        - ``triangle`` or ``t``
        - ``sTriangle`` or ``st`` (solid)
        - ``square`` or ``s``
        - ``sSquare`` or ``ss`` (solid)
        - ``circle`` or ``c``
        - ``sCircle`` or ``sc`` (solid)
        - ``none`` or ``n``
      - Define current transition arrow. Default is ``arrow``

.. warning:: Changing transition interpolation types in the client view will not affect transitions which has the ``type`` option set to a given value.

Example
.......

In this example, the configuration used for transition from **State B** to **State C** is:

    ``{style:dotted, type:step, head:circle}``

And for the transition from **State C** to **State B**, the configuration is:

    ``{head:ss}``

And the resulting graph is below

.. image:: img/trans-config.png