# Prerequisites
Before the installation of **Workflow Dashboard** to any of your vaults, please check the following prerequisites:

1. Make a full backup of your vault, so that you can revert to this stage, if anything goes wrong! (https://www.m-files.com/user-guide/latest/eng/Operations_Backup.html)
2. If not already done, download the `WorkflowDashboard.<VERSION>.mfappx` file.
3. Have Administrator rights to the vault
4. Make sure that your license file is available, otherwise the application will run as trial.
# Installing the application
To install the MFAPPX Vault application, please follow the following steps:

1. Start M-Files Admin.
2. Select the target vault.
3. Right-click on the vault name, choose `Applications`
4. In the dialog choose `Install…`
5. Select the application file with the `.mfappx` extension.
6. Answer `yes` to the question `Do you want to restart the document vault now`.

# How to find my M-Files Serial Number?
To know where to find your M-Files Serial Number, please fellow the steps at this [Link](https://m-files.force.com/s/article/From-where-can-I-find-my-M-Files-Serial-number)

# How to find my Vault GUID?
To find your M-Files Vault GUID or Unique ID, simply right click on the Vault in M-Files Admin, then click on `Properties` in the context menu, the Vault Unique ID is under the Name in General tab.

# How to find my Instance ID?
1. Right click on the vault where the Workflow Dashboard is installed
2. Click on `Applications`
3. Select `Workflow Dashboard` from the applications list
4. Click on `License` button
5. The Instance ID will be displayed on the License Information pane

![License ID](img/instance-id.jpg)

# Installing the license
1. Start M-Files Admin
2. Select the vault where Workflow Dashboard is installed
3. Right-click on the vault name, choose `Applications`
4. Select `Workflow Dahsboard` application in the list
5. Click on `License…` and select the `.lic` file that was provided.
6. Apply the license.